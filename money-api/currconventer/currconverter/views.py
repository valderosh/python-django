import requests
from bs4 import BeautifulSoup as bs
from django.shortcuts import render

def get_exchange_rates():
    url = 'https://bank.gov.ua/ua/markets/exchangerates'
    result = requests.get(url)
    soup = bs(result.content, 'html.parser')
    table = soup.find('table', {'class': 'exchange-rate-table'})
    rows = table.find_all('tr')

    exchange_rates = {}
    for row in rows:
        cells = row.find_all('td')
        currency = cells[1].text
        rate = cells[3].text.replace(',', '.')
        exchange_rates[currency] = float(rate)

    return exchange_rates

def currency_score(request):
    exchange_rates = get_exchange_rates()
    return render(request, 'currency.html', {'exchange_rates': exchange_rates})


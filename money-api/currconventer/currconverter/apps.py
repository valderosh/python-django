from django.apps import AppConfig


class CurrconverterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'currconverter'

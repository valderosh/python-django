from django.db import models

class Image(models.Model):
    title = models.CharField(max_length=100, verbose_name='Title')
    image = models.ImageField(upload_to='images', verbose_name='Image')


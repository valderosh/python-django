from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from PIL import Image


def get_image_list(request):
    images = Image.objects.all()
    return render(request, 'image_list.html', {'images': images})


def get_image_detail(request, id):
    image = get_object_or_404(Image, id=id)
    return render(request, 'image_detail.html', {'image': image})


def post_image(request):
    if request.method == 'POST' and request.FILES['image']:
        image_file = request.FILES['image']
        fs = FileSystemStorage()
        filename = fs.save(image_file.name, image_file)
        #prewiew
        im = Image.open(image_file)
        im.thumbnail((100, 100))
        preview_filename = 'preview-' + filename
        im.save(settings.MEDIA_ROOT + '/' + preview_filename, 'png')
        return redirect('image_list')
    return render(request, 'upload_image.html')


def delete_image(request, id):
    image = get_object_or_404(Image, id=id)
    image.delete()
    return redirect('image_list')

def get_image_preview(request, image_id):
    image = get_object_or_404(Image, id=image_id)
    with open(image.image.path, 'rb') as f:
        img = Image.open(f)
        img.thumbnail((100, 100))
        response = HttpResponse(content_type='image/png')
        img.save(response, 'PNG')
    return response


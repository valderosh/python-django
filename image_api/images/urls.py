from django.urls import path
from .views import get_image_preview, get_image_list, get_image_detail

urlpatterns = [
    path('/', get_image_list, name='image_list'),
    path('images/<int:pk>/', get_image_detail, name='image_detail'),
    path('images/<int:pk>/preview/', get_image_preview, name='image_preview')]